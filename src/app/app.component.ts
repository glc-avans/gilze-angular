import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent  { abbreviation = 'F.R.A.U.D.S.'; name = 'Flight Registration and Automated Unit Detection System'; }
