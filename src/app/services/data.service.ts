import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { Http, Response, Headers } from '@angular/http';

import { Flight } from './../classes/flight.class';
import { User } from './../classes/user.class';

@Injectable()
export class DataService {

  url:string = "";

  constructor(private http : Http){
  }

  load() : Promise<Object>
  {
  	return new Promise((resolve, reject) => {

  		if(this.url != "")
  		{
  			resolve();
  		}
  		else
  		{
  			this.http.get('/app/config/api.json').subscribe((res)=>{
		  		 var data = res.json();
			     this.url = data.protocol + data.hostname + ":" + data.port + "/" + data.api;
			     console.log(this.url);
			     resolve();
			});
  		}
  		
  	});
  	
  }

  flights(): Observable<Flight[]> {
    let flightdata = this.http.get(`${this.url}/flights`, {headers: this.getHeaders()}).map(r => r.json());
    return flightdata;
  }

  users(): Observable<User[]> {
    let usersdata = this.http.get(`${this.url}/users`, {headers: this.getHeaders()}).map(r => r.json());
    return usersdata;
  }


  private getHeaders(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
}