import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';


@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})

export class FlightsComponent implements OnInit { 

	temp:string = "Loading...";

	constructor(private data:DataService){}

	getFlightsData(): void {
		this.data.load().then(()=> {
			this.data
		      .flights()
		      .subscribe(b => this.temp = JSON.stringify(b));
		});
	}

	ngOnInit(): void {
	    this.getFlightsData();
	}

}
