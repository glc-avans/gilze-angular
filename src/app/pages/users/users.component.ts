import { Component } from '@angular/core';
import { DataService } from './../../services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent  { 

	temp:string = "Loading...";

	constructor(private data:DataService){}

	getUsersData(): void {
		this.data.load().then(()=> {
			this.data
		      .users()
		      .subscribe(b => this.temp = JSON.stringify(b));
		});
	}

	ngOnInit(): void {
	    this.getUsersData();
	}
}
